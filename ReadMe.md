# Bees'Nest Simulator

Un simulateur d'entreprise pour les travaux pratiques des formations en Économie-Gestion, développé autour des technologies Java Web J2E.

## Jeu Économique d'Entreprises en J2E SpringBoot / Tomcat

Bees'Nest est un projet de jeu d'entreprise, un simulateur économique à destination des enseignants et professeurs de gestion.

Réalisé en Java EE, il embarque le serveur Tomcat grâce à SpringBoot, et permet la sauvegarde et le restauration des parties en JSON, directement depuis le bureau du professeur.

Le serveur permet aux étudiants de se connecter au jeu depuis leur ordinateur ou leur smartphone, et d'accéder aux données et statistiques de leur entreprise au travers d'une interface Web.

L'enseignant dispose d'une interface Web de gestion du jeu et des participants.
