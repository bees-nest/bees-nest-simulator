/**
 * Copyleft 2019 - Vincent Mahé - vmahe@free.fr
 * Eclipse Public License 1.0
 */
package biz.nest.simulator.services.admin;

import java.util.Collection;

import javax.validation.Valid;

import org.springframework.stereotype.Service;

import biz.nest.simulator.modele.Modele;
import biz.nest.simulator.modele.jeu.Equipe;
import biz.nest.simulator.modele.jeu.Etudiant;

/**
 * @author Vincent Mahé
 *
 */
@Service
public class AdminEquipe {
	
	public Equipe ajouterEquipe(@Valid Equipe equipe) {
		Modele.getInstance().add(equipe);
		return equipe;
	}
	
	public Collection<Equipe> listerEquipes() {
		return Modele.getInstance().getEquipes().values();
	}

	public void supprimerEquipe(Integer id) {
		Collection<Etudiant> etudiants = Modele.getInstance().getEquipes().get(id).getEtudiants();
		Modele.getInstance().removeEquipe(id);
		for (Etudiant etudiant : etudiants) {
			Modele.getInstance().removeEtudiant(etudiant.getId());
		}
	}

	/* Étudiants de l'équipe */
	
	public Etudiant ajouterEtudiant(@Valid Etudiant etudiant, Equipe equipe) {
		Modele.getInstance().add(etudiant);
		
		equipe.addEtudiant(etudiant);
		
		return etudiant;
	}
	
	public Collection<Etudiant> listerEtudiants() {
		return Modele.getInstance().getEtudiants().values();
	}
	
	public void supprimerEtudiant(Integer idEtudiant, Integer idEquipe) {
		Etudiant etudiant = Modele.getInstance().getEtudiants().get(idEtudiant);
		
		// on l'enlève d'abord de son équipe
		this.getById(idEquipe).getEtudiants().remove(etudiant);
		
		// on l'efface ensuite de la base
		Modele.getInstance().removeEtudiant(idEtudiant);
	}
	
	// nécessaire au parser d'équipe (EquipeFormateur)
	public Equipe getById(Integer id) {
		return Modele.getInstance().getEquipes().get(id);
	}
	
	public Integer getNextId() {
		return Modele.getInstance().getNextId();
	}
}
