/**
 * Copyleft 2019 - Vincent Mahé - vmahe@free.fr
 * Eclipse Public License 1.0
 */
package biz.nest.simulator.services.admin;

import org.springframework.stereotype.Service;

import biz.nest.simulator.modele.Modele;

/**
 * @author Vincent Mahé
 *
 */
@Service
public class AdminEconomie {
	
	public Modele getEconomie() {
		return Modele.getInstance();
	}
	
	public void setEconomie(Modele modele) {
		Modele.setInstance(modele);
	}
}
