/**
 * Copyleft 2019 - Vincent Mahé - vmahe@free.fr
 * Eclipse Public License 1.0
 */
package biz.nest.simulator.services.admin;

import java.util.ArrayList;
import java.util.Collection;

import javax.validation.Valid;

import org.springframework.stereotype.Service;

import biz.nest.simulator.modele.Modele;
import biz.nest.simulator.modele.economie.Entreprise;
import biz.nest.simulator.modele.jeu.Equipe;

/**
 * @author Vincent Mahé
 *
 */
@Service
public class AdminEntreprise {

	public Entreprise ajouterEntreprise(@Valid Entreprise entreprise) {
		Modele.getInstance().add(entreprise);
		return entreprise;
	}
	
	public Collection<Entreprise> listerEntreprises() {
		return Modele.getInstance().getEntreprises().values();
	}
	
	public Collection<Entreprise> listerEntreprisesDisponibles() {
		Collection<Entreprise> entreprises = new ArrayList<Entreprise>(listerEntreprises());
		for (Equipe equipe : Modele.getInstance().getEquipes().values()) {
			entreprises.remove(equipe.getEntreprise());
		}
		return entreprises;
	}

	public void supprimerEntreprise(Integer id) {
		Modele.getInstance().removeEntreprise(id);
	}
	
	// nécessaire au parser d'équipe (EquipeFormateur)
	public Entreprise getById(Integer id) {
		return Modele.getInstance().getEntreprises().get(id);
	}
	
	public Integer getNextId() {
		return Modele.getInstance().getNextId();
	}
}
