/**
 * Copyleft 2019 - Vincent Mahé - vmahe@free.fr
 * Eclipse Public License 1.0
 */
package biz.nest.simulator.services.result;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.springframework.stereotype.Service;

import biz.nest.simulator.modele.Modele;
import biz.nest.simulator.modele.economie.Entreprise;
import biz.nest.simulator.modele.economie.compta.Exercice;
import biz.nest.simulator.modele.economie.compta.ResultatsExercice;
import biz.nest.simulator.modele.jeu.Equipe;

/**
 * @author Vincent Mahé
 *
 */
@Service
public class ResultatsEntreprise {

	/**
	 * @param id
	 * @return
	 */
	public Equipe getEquipe(Integer id) {
		return Modele.getInstance().getEquipes().get(id);
	}

	/* VM : bouchon fournissant des résultats en attendant la simulation
	   On teste quand même que l'entreprise n'a pas déjà été "comblée" ;-) */
	public void setResultats(Entreprise entreprise) {
		if (entreprise.getResultats().isEmpty()) {
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			Exercice ex2017 = null;
			Exercice ex2018 = null;
			Exercice ex2019 = null;
			try {
				ex2017 = new Exercice(sdf.parse("01/01/2017"), sdf.parse("31/12/2017"), "2017");
				ex2018 = new Exercice(sdf.parse("01/01/2018"), sdf.parse("31/12/2018"), "2018");
				ex2019 = new Exercice(sdf.parse("01/01/2019"), sdf.parse("31/12/2019"), "2019");
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			ResultatsExercice r2017 = new ResultatsExercice(ex2017, 1500000, 100000);
			ResultatsExercice r2018 = new ResultatsExercice(ex2018, 2000000, 150000);
			ResultatsExercice r2019 = new ResultatsExercice(ex2019, 2300000,  80000);
			
			entreprise.addResultat(r2017);
			entreprise.addResultat(r2018);
			entreprise.addResultat(r2019);
		}
	}
}
