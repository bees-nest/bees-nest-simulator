/**
 * Copyleft 2019 - Vincent Mahé - vmahe@free.fr
 * Eclipse Public License 1.0
 */
package biz.nest.simulator.modele;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonProperty;

import biz.nest.simulator.modele.economie.Demande;
import biz.nest.simulator.modele.economie.Entreprise;
import biz.nest.simulator.modele.economie.Fournisseur;
import biz.nest.simulator.modele.economie.Produit;
import biz.nest.simulator.modele.jeu.Equipe;
import biz.nest.simulator.modele.jeu.Etudiant;
import biz.nest.simulator.modele.simulation.Partie;

/**
 * @author Vincent Mahé
 * 
 * Classe prévue pour englober l'ensemble des objets créés dans le jeu.
 * Elle permet de stocker, sérialiser vers un fichier, et restaurer le jeu.
 */
public class Modele {

	private static Modele instance = new Modele();
	
	private Partie partie;
	private Demande demande;
	private Produit produit;
	
	/* Gestion des Map par Jackson => les spécifier explicitement pour pouvoir les restaurer */
	@JsonProperty("")
	private Map<Integer, Entreprise> entreprises = new HashMap<Integer, Entreprise>();
	@JsonProperty("")
	private Map<Integer, Fournisseur> fournisseurs = new HashMap<Integer, Fournisseur>();
	
	@JsonProperty("")
	private Map<Integer, Etudiant> etudiants = new HashMap<Integer, Etudiant>();
	@JsonProperty("")
	private Map<Integer, Equipe> equipes = new HashMap<Integer, Equipe>();
	
	private boolean aSauvegarder = false;
	
	/**
	 * @return
	 */
	public static Modele getInstance() {
		return instance;
	}
	/**
	 * @return
	 */
	public static void setInstance(Modele modele) {
		instance = modele;
	}

	public Partie getPartie() {
		return partie;
	}
	public void setPartie(Partie partie) {
		this.partie = partie;
	}
	public void setNouvellePartie(Partie partie) {
		setPartie(partie);;
		// on purge les données de la partie précédente
		setEntreprises(new HashMap<>());
		setEquipes(new HashMap<>());
		setEtudiants(new HashMap<>());
		setFournisseurs(new HashMap<>());
	}

	/**
	 * @param entreprise
	 */
	public void add(Entreprise entreprise) {
		entreprises.put(entreprise.getId(), entreprise);
		this.aSauvegarder = true;
	}
	/**
	 * @param id
	 */
	public void removeEntreprise(Integer id) {
		entreprises.remove(id);
		this.aSauvegarder = true;
	}
	/**
	 * @return the demande
	 */
	public Demande getDemande() {
		return demande;
	}
	/**
	 * @param demande the demande to set
	 */
	public void setDemande(Demande demande) {
		this.demande = demande;
		this.aSauvegarder = true;
	}
	/**
	 * @return the produit
	 */
	public Produit getProduit() {
		return produit;
	}
	/**
	 * @param produit the produit to set
	 */
	public void setProduit(Produit produit) {
		this.produit = produit;
		this.aSauvegarder = true;
	}
	/**
	 * @return the entreprises
	 */
	public Map<Integer, Entreprise> getEntreprises() {
		return entreprises;
	}
	/**
	 * @param entreprises the entreprises to set
	 */
	public void setEntreprises(Map<Integer, Entreprise> entreprises) {
		this.entreprises = entreprises;
		this.aSauvegarder = true;
	}
	/**
	 * @return the fournisseurs
	 */
	public Map<Integer, Fournisseur> getFournisseurs() {
		return fournisseurs;
	}
	/**
	 * @param fournisseurs the fournisseurs to set
	 */
	public void setFournisseurs(Map<Integer, Fournisseur> fournisseurs) {
		this.fournisseurs = fournisseurs;
		this.aSauvegarder = true;
	}
	/**
	 * @return the etudiants
	 */
	public Map<Integer, Etudiant> getEtudiants() {
		return etudiants;
	}
	/**
	 * @param etudiant
	 */
	public void add(Etudiant etudiant) {
		this.etudiants.put(etudiant.getId(), etudiant);
		this.aSauvegarder = true;
	}
		/**
	 * @param etudiants the etudiants to set
	 */
	public void setEtudiants(Map<Integer, Etudiant> etudiants) {
		this.etudiants = etudiants;
		this.aSauvegarder = true;
	}
	/**
	 * @param idEtudiant
	 */
	public void removeEtudiant(Integer id) {
		etudiants.remove(id);
		this.aSauvegarder = true;
	}
	/**
	 * @return the equipes
	 */
	public Map<Integer, Equipe> getEquipes() {
		return equipes;
	}
	/**
	 * @param equipes the equipes to set
	 */
	public void setEquipes(Map<Integer, Equipe> equipes) {
		this.equipes = equipes;
	}
	/**
	 * @param etudiant
	 */
	public void add(Equipe equipe) {
		this.equipes.put(equipe.getId(), equipe);
		this.aSauvegarder = true;
	}
	/**
	 * @param equipe
	 */
	public void removeEquipe(Integer id) {
		equipes.remove(id);
		this.aSauvegarder = true;
	}
	
	// managing IDs as we do not use a database for storage
	private Integer nextId = 0;
	public Integer getNextId() {
		nextId++;
		return nextId;
	}
	/* pour la restauration depuis la sauvegarde JSON */
	public void setNextId(Integer nextId) {
		this.nextId = nextId;
	}
	public boolean estASauvegarder() {
		return aSauvegarder;
	}
}
