/**
 * Copyleft 2019 - Vincent Mahé - vmahe@free.fr
 * Eclipse Public License 1.0
 */
package biz.nest.simulator.modele.economie;

/**
 * @author Vincent Mahé
 * 
 * L'ibjet Demande fournit la quantité d'achats correspondant à un prix donné
 * via une fonction de demande de type log-normale.
 * Il n'est donc pas sérialisé dans la base de données.
 */
public class Demande {

	public int getDemande(Produit produit, double prix) {
		return 0;
	}
}
