/**
 * Copyleft 2019 - Vincent Mahé - vmahe@free.fr
 * Eclipse Public License 1.0
 */
package biz.nest.simulator.modele.jeu;

import java.io.Serializable;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.validation.constraints.NotEmpty;

import org.springframework.beans.factory.annotation.Value;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import biz.nest.simulator.modele.economie.Entreprise;
import biz.nest.simulator.util.OutilsServeur;

/**
 * @author Vincent Mahé
 * 
 * Classe regroupant les étudiants gérant ensemble une entreprise au sein du jeu.
 */
@JsonIgnoreProperties("mail")  // cette propriété est recalculée à chaque changement d'étudiant
public class Equipe implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer id;
	
	@NotEmpty
	private String pseudo;
	
	/**
	 * champ un peu artificiel, pour bénéficier du mappage automatique
	 * des propriétés des objets dans HTML par Thymeleaf...
	 */
	private String mail;
	
	private Entreprise entreprise;
	
	/**
	 * 
	 */
	public Equipe() { }
	
	/**
	 * @return the pseudo
	 */
	public String getPseudo() {
		return pseudo;
	}

	/**
	 * @param pseudo
	 */
	public Equipe(@NotEmpty String pseudo) {
		super();
		this.pseudo = pseudo;
	}

	/**
	 * @param pseudo the pseudo to set
	 */
	public void setPseudo(String pseudo) {
		this.pseudo = pseudo;
	}

	private Collection<Etudiant> etudiants = new ArrayList<Etudiant>();
	
	public void addEtudiant(Etudiant e) {
		// l'étudiant a son équipe dès sa création dans la page HTML
		etudiants.add(e);
		
		// rafraîchir le mail de l'équipe
		genererMail();
	}
	
	public Collection<Etudiant> getEtudiants() {
		return etudiants;
	}
	

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public Entreprise getEntreprise() {
		return entreprise;
	}

	public void setEntreprise(Entreprise entreprise) {
		this.entreprise = entreprise;
	}

    @Value("${server.port}")
    private String port;

	/**
	 * Méthode générant le mail à envoyer aux étudiants de l'équipe
	 * qui leur donne le lien sur lequel cliquer pour accéder à leur partie.
	 * 
	 * @return le champ HTML complet du "mailTo" à figurer dans le tableau des équipes
	 */
	public void genererMail() {
		
		// génération du mail
		String objet = "Bees'Nest - connexion Équipe " + this.getPseudo();
		//TODO ajouter le code spécifique à chaque équipe
		String corps = "";
		try {
			corps = "Pour vous connecter, cliquez sur le lien suivant : "
					+ OutilsServeur.adresseServeur() + ":"
					+ OutilsServeur.portServeur()
					+ "/Connexion?id=" + this.getId();
		} catch (SocketException e) {
			// TODO gérer l'erreur potentielle
			// Rem : utiliser le corps du mail pour informer le prof du problème rencontré
			e.printStackTrace();
		}
		String adresses = "";
		Iterator<Etudiant> etudiants = this.getEtudiants().iterator();
		while (true) {
			adresses += etudiants.next().getEmail();
			if (etudiants.hasNext())
				adresses += ";";
			else
				break;
		}
		this.setMail("mailto:" + adresses + "?subject=" + objet + "&body=" + corps);
	}
}
