/**
 * Copyleft 2019 - Vincent Mahé - vmahe@free.fr
 * Eclipse Public License 1.0
 */
package biz.nest.simulator.modele.simulation;

/**
 * @author Vincent Mahé
 *
 */
public class Partie {

	private String nom;

	/**
	 * 
	 */
	public Partie() {
	}
	
	/**
	 * @param nom
	 */
	public Partie(String nom) {
		super();
		this.nom = nom;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}
	
}
