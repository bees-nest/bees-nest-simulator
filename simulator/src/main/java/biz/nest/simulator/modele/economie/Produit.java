/**
 * Copyleft 2019 - Vincent Mahé - vmahe@free.fr
 * Eclipse Public License 1.0
 */
package biz.nest.simulator.modele.economie;

import java.io.Serializable;

import javax.validation.constraints.NotEmpty;

/**
 * @author Vincent Mahé
 * 
 * Classe représentant les produits échangés entre entreprisess et avec les consommateurs.
 * Certains sont des matières premières, d'autres des produits intermédiaires,
 * d'autres des produits finis, et certains peuvent être de simples services. 
 */
public class Produit implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	public Produit() { }


	private Integer id;
	@NotEmpty
	private String nom;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
}
