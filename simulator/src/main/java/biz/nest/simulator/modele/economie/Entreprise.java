/**
 * Copyleft 2019 - Vincent Mahé - vmahe@free.fr
 * Eclipse Public License 1.0
 */
package biz.nest.simulator.modele.economie;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

import javax.validation.constraints.NotEmpty;

import biz.nest.simulator.modele.economie.compta.ResultatsExercice;


/**
 * @author Vincent Mahé
 * 
 * Classe représentant les entreprises actrices du jeu (y compris les fournisseurs)
 */
public class Entreprise implements Serializable {
	
	private static final long serialVersionUID = 1L;

	public Entreprise() { }

	private Integer id;
	@NotEmpty
	private String nom;
	
	private Collection<ResultatsExercice> resultats = new ArrayList<ResultatsExercice>();
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	
	/**
	 * @param nom
	 */
	public Entreprise(String nom) {
		super();
		this.nom = nom;
	}

	public Collection<ResultatsExercice> getResultats() {
		return resultats;
	}
	public void addResultat(ResultatsExercice resultat) {
		this.resultats.add(resultat);
	}
//	public ResultatsExercice getResultat(Date dateDebut) {
//		return resultats.get(dateDebut);
//	}
}
