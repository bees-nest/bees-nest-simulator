/**
 * Copyleft 2019 - Vincent Mahé - vmahe@free.fr
 * Eclipse Public License 1.0
 */
package biz.nest.simulator.modele.economie.compta;

/**
 * @author Vincent Mahé
 *
 */
public class ResultatsExercice {

	private Exercice exercice;
	private Integer chiffreAffaires;	// on ne s'occupera pas des centimes...
	private Integer benefice;			// si pertes, nombre négatif
	
	/**
	 * 
	 */
	public ResultatsExercice() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @return the exercice
	 */
	public Exercice getExercice() {
		return exercice;
	}

	/**
	 * @param exercice the exercice to set
	 */
	public void setExercice(Exercice exercice) {
		this.exercice = exercice;
	}

	/**
	 * @return the chiffreAffaires
	 */
	public Integer getChiffreAffaires() {
		return chiffreAffaires;
	}

	/**
	 * @param chiffreAffaires the chiffreAffaires to set
	 */
	public void setChiffreAffaires(Integer chiffreAffaires) {
		this.chiffreAffaires = chiffreAffaires;
	}

	/**
	 * @return the benefice
	 */
	public Integer getBenefice() {
		return benefice;
	}

	/**
	 * @param benefice the benefice to set
	 */
	public void setBenefice(Integer benefice) {
		this.benefice = benefice;
	}

	/**
	 * @param exercice
	 * @param chiffreAffaires
	 * @param benefice
	 */
	public ResultatsExercice(Exercice exercice, Integer chiffreAffaires, Integer benefice) {
		super();
		this.exercice = exercice;
		this.chiffreAffaires = chiffreAffaires;
		this.benefice = benefice;
	}
	
}
