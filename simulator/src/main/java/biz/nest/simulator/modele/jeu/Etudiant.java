/**
 * Copyleft 2019 - Vincent Mahé - vmahe@free.fr
 * Eclipse Public License 1.0
 */
package biz.nest.simulator.modele.jeu;

import java.io.Serializable;

import javax.validation.constraints.NotEmpty;

/**
 * @author Vincent Mahé
 * 
 * Entité individuelle participant à la gestion d'une des entreprises du jeu.
 * Elle ne peut la gérer qu'au sein d'une Equipe.
 */
public class Etudiant implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	public Etudiant() { }

	private Integer id;
	@NotEmpty
	private String nom;
	@NotEmpty
	private String email;
	
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	private Integer idEquipe;

	public Integer getIdEquipe() {
		return idEquipe;
	}

	public void setIdEquipe(Integer idEquipe) {
		this.idEquipe = idEquipe;
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	
}
