/**
 * Copyleft 2019 - Vincent Mahé - vmahe@free.fr
 * Eclipse Public License 1.0
 */
package biz.nest.simulator.modele.economie.compta;

import java.util.Date;

/**
 * @author Vincent Mahé
 *
 */
public class Exercice {

	private Date debut;
	private Date fin;
	private String code;
	
	/**
	 * 
	 */
	public Exercice() {
	}

	/**
	 * @return the debut
	 */
	public Date getDebut() {
		return debut;
	}

	/**
	 * @param debut the debut to set
	 */
	public void setDebut(Date debut) {
		this.debut = debut;
	}

	/**
	 * @return the fin
	 */
	public Date getFin() {
		return fin;
	}

	/**
	 * @param fin the fin to set
	 */
	public void setFin(Date fin) {
		this.fin = fin;
	}

	/**
	 * @return the code
	 */
	public String getCode() {
		return code;
	}

	/**
	 * @param code the code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * @param debut
	 * @param fin
	 * @param code
	 */
	public Exercice(Date debut, Date fin, String code) {
		super();
		this.debut = debut;
		this.fin = fin;
		this.code = code;
	}
}
