/**
 * Copyleft 2019 - Vincent Mahé - vmahe@free.fr
 * Eclipse Public License 1.0
 */
package biz.nest.simulator.util;

import java.text.ParseException;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.Formatter;
import org.springframework.stereotype.Service;

import biz.nest.simulator.modele.economie.Entreprise;
import biz.nest.simulator.services.admin.AdminEntreprise;

/**
 * @author Vincent Mahé
 *
 * Classe necéssaire  à la transformation d'un ID en entreprise
 * dans la page AdminEntreprise.html (éléemsnt SELECT).
 * Bien penser à inscrire ce service dans le WebConfig !!
 */
@Service
public class EntrepriseFormateur implements Formatter<Entreprise> {

	@Autowired
	AdminEntreprise admin;

	@Override
	public String print(Entreprise object, Locale locale) {
		return (object != null ? object.getId().toString() : "");
	}

	@Override
	public Entreprise parse(String text, Locale locale) throws ParseException {
		Integer id = Integer.valueOf(text);
		return this.admin.getById(id);
	}
	
	
}
