/**
 * Copyleft 2019 - Vincent Mahé - vmahe@free.fr
 * Eclipse Public License 1.0
 */
package biz.nest.simulator.util;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;

/**
 * 
 * @author Vincent Mahé
 *
 */
public class OutilsServeur {
	
	public static String adresseServeur() throws SocketException {
		String ipAdresse = null;

		Enumeration<NetworkInterface> networkInterfaces = NetworkInterface.getNetworkInterfaces();  // gets All networkInterfaces of your device
        while (networkInterfaces.hasMoreElements()) {
            NetworkInterface inet = (NetworkInterface) networkInterfaces.nextElement();
            Enumeration<InetAddress> address = inet.getInetAddresses();
            while (address.hasMoreElements()) {
                InetAddress inetAddress = (InetAddress) address.nextElement();
                if (inetAddress.isSiteLocalAddress()) {
                    ipAdresse = inetAddress.getHostAddress();
                }
            }
        }

        //TODO passer en https
        return "http://" + ipAdresse;
	}

	/**
	 * @return
	 */
	public static String portServeur() {
		// NB : fixé dans le fichier application.properties
		return "12005";
	}
}
