/**
 * Copyleft 2019 - Vincent Mahé - vmahe@free.fr
 * Eclipse Public License 1.0
 */
package biz.nest.simulator.util;

import java.text.ParseException;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.Formatter;
import org.springframework.stereotype.Service;

import biz.nest.simulator.modele.jeu.Equipe;
import biz.nest.simulator.services.admin.AdminEquipe;

/**
 * @author Vincent Mahé
 *
 * Classe necéssaire  à la transformation d'un ID en équipe
 * dans la page AdminEquipe.html (éléemsnt SELECT).
 * Bien penser à inscrire ce service dans le WebConfig !!
 */
@Service
public class EquipeFormateur implements Formatter<Equipe> {

	@Autowired
	AdminEquipe admin;

	@Override
	public String print(Equipe object, Locale locale) {
		return (object != null ? object.getId().toString() : "");
	}

	@Override
	public Equipe parse(String text, Locale locale) throws ParseException {
		Integer id = Integer.valueOf(text);
		return this.admin.getById(id);
	}
	
	
}
