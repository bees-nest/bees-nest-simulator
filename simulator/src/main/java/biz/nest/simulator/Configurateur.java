/**
 * Copyleft 2019 - Vincent Mahé - vmahe@free.fr
 * Eclipse Public License 1.0
 */
package biz.nest.simulator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.format.FormatterRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import biz.nest.simulator.util.EntrepriseFormateur;
import biz.nest.simulator.util.EquipeFormateur;

/**
 * @author Vincent Mahé
 *
 */
@Configuration
//@EnableWebMvc
//@ComponentScan(value = {"biz.nest.simulator.modele.jeu"})
public class Configurateur implements WebMvcConfigurer {

	// formateurs de types
	@Autowired
	private EquipeFormateur equipeForm;

	// formateurs de types
	@Autowired
	private EntrepriseFormateur entrepriseForm;
	
	@Override
	public void addFormatters(FormatterRegistry registry) {
		registry.addFormatter(entrepriseForm);
		registry.addFormatter(equipeForm);
	}
}
