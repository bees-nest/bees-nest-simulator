/**
 * Copyleft 2019 - Vincent Mahé - vmahe@free.fr
 * Eclipse Public License 1.0
 */
package biz.nest.simulator.ihm.professeur;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @author Vincent Mahé
 *
 */
@Controller
public class AdministrationControleur {

	@GetMapping("/Administration")
	public String adminPartie(Model model) {
		return "Administration";
	}
}
