/**
 * Copyleft 2019 - Vincent Mahé - vmahe@free.fr
 * Eclipse Public License 1.0
 */
package biz.nest.simulator.ihm.professeur;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import biz.nest.simulator.modele.economie.Entreprise;
import biz.nest.simulator.services.admin.AdminEntreprise;

/**
 * @author Vincent Mahé
 *
 */
@Controller
public class EntrepriseControleur {
	
	@Autowired
	AdminEntreprise adminEntreprise;
	
	// entering admnistrative page
	@GetMapping("/AdminEntreprise")
	public String adminEntreprise(Model model) {
		return adminEntrepriseSetUp(model);
	}

	@PostMapping("/AdminEntreprise")
	public String ajouterEntreprise(
			@Valid Entreprise entreprise,
			BindingResult result,
			Model model) {
		if (result.hasErrors()) {
			//TODO manage errors
			return adminEntrepriseSetUp(model);
		}
		entreprise.setId(adminEntreprise.getNextId());
		adminEntreprise.ajouterEntreprise(entreprise);
		
		return adminEntrepriseSetUp(model);
	}
	
	@GetMapping("/SupprimerEntreprise")
	public String supprimerEntreprise(
			@RequestParam(value = "id", required = true) Integer id,
			Model model) {
		adminEntreprise.supprimerEntreprise(id);
		return adminEntrepriseSetUp(model);
	}
	
	private String adminEntrepriseSetUp(Model model) {
		// clean-up the textbox
		model.addAttribute("entreprise", new Entreprise());
		model.addAttribute("entreprises", adminEntreprise.listerEntreprises());
		return "AdminEntreprise";
	}
}
