/**
 * Copyleft 2019 - Vincent Mahé - vmahe@free.fr
 * Eclipse Public License 1.0
 */
package biz.nest.simulator.ihm.etudiant;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import biz.nest.simulator.modele.jeu.Equipe;
import biz.nest.simulator.services.result.ResultatsEntreprise;

/**
 * @author Vincent Mahé
 *
 */
@Controller
public class AffichageEquipeControleur {

	@Autowired
	ResultatsEntreprise result;
	
	//TODO récupérer l'ID objet de l'équipe pour traiter les requêtes d'affichage des étudiants
	@GetMapping("/Connexion")
	public String connexion(
			@RequestParam(value = "id", required = true) Integer id,
			Model model) {
		Equipe equipe = result.getEquipe(id);
		
		// bouchon
		result.setResultats(equipe.getEntreprise());
		// fin bouchon
		
		return setupPage(model, equipe);
	}

	/**
	 * @param model
	 * @return
	 */
	private String setupPage(Model model, Equipe equipe) {
		if (equipe == null)
			return "erreur";
		model.addAttribute("equipe", equipe);
		return "ResultatsPrincipaux";
	}
}
