/**
 * Copyleft 2019 - Vincent Mahé - vmahe@free.fr
 * Eclipse Public License 1.0
 */
package biz.nest.simulator.ihm.professeur;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import biz.nest.simulator.modele.Modele;
import biz.nest.simulator.modele.jeu.Equipe;
import biz.nest.simulator.services.admin.AdminEconomie;

/**
 * @author Vincent Mahé
 *
 */
@Controller
public class SauvegardeControleur {
	
	@Autowired
	AdminEconomie adminEco;
	
	// we build the JSON for a direct download to user browser
	@GetMapping("/AdminSauver")
	public String sauvegarder(Model model) {
		ObjectMapper mapper = new ObjectMapper();
		
		String base = "";
		try {
			base = mapper.writeValueAsString(Modele.getInstance());
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		model.addAttribute("base", base);
		return "AdminSauver";
	}
	
	// entry for restoring base
	@GetMapping("/AdminRestaurer")
	public String selectionner(Model model) {
		return "AdminRestaurer";
	}
	
	// the user has selected a file
	@PostMapping("/AdminRestaurer")
	public String restaurer(
			@RequestParam(value = "baseJson", required = true) String baseJson,
			Model model) {
		
		//TODO vérifier qu'une partie n'est pas déjà en cours
		
		ObjectMapper mapper = new ObjectMapper();
		try {
			Modele economie = mapper.readValue(baseJson, Modele.class);
			Modele.setInstance(economie);
			
			// on doit regénérer, pour chaque équipe, le mail de connexion des étudiants
			for (Equipe equipe : Modele.getInstance().getEquipes().values()) {
				equipe.genererMail();
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return "AdminRestaurer";
	}
}
