/**
 * Copyleft 2019 - Vincent Mahé - vmahe@free.fr
 * Eclipse Public License 1.0
 */
package biz.nest.simulator.ihm.professeur;

import java.util.List;
import java.util.Set;

import org.springframework.beans.BeansException;
import org.springframework.boot.ApplicationArguments;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @author Vincent Mahé
 *
 */
@Controller
public class ArretServeurControleur implements ApplicationContextAware, ApplicationArguments {

	private ApplicationContext context;
	
	@GetMapping("/ArretServeur")
	public void arretServeur() {
		((ConfigurableApplicationContext) context).close();
	}
	
	@Override
	public String[] getSourceArgs() {
		return null;
	}

	@Override
	public Set<String> getOptionNames() {
		return null;
	}

	@Override
	public boolean containsOption(String name) {
		return false;
	}

	@Override
	public List<String> getOptionValues(String name) {
		return null;
	}

	@Override
	public List<String> getNonOptionArgs() {
		return null;
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.context = applicationContext;
	}

}
