/**
 * Copyleft 2019 - Vincent Mahé - vmahe@free.fr
 * Eclipse Public License 1.0
 */
package biz.nest.simulator.ihm.professeur;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import biz.nest.simulator.modele.jeu.Equipe;
import biz.nest.simulator.modele.jeu.Etudiant;
import biz.nest.simulator.services.admin.AdminEntreprise;
import biz.nest.simulator.services.admin.AdminEquipe;

/**
 * @author Vincent Mahé
 *
 */
@Controller
public class EquipeControleur {
	
	@Autowired
	AdminEquipe adminEquipe;
	
	@Autowired
	AdminEntreprise adminEntreprise;
	
	// entering admnistrative page
	@GetMapping("/AdminEquipe")
	public String adminEquipe(Model model) {
		return pageAdminEquipeSetUp(model);
	}

	@PostMapping("/AdminEquipe")
	public String ajouterEquipe(
			@Valid Equipe equipe,
			BindingResult result,
			Model model) {
		if (result.hasErrors()) {
			//TODO manage errors
			return pageAdminEquipeSetUp(model);
		}
		equipe.setId(adminEquipe.getNextId());
		adminEquipe.ajouterEquipe(equipe);
		
		return pageAdminEquipeSetUp(model);
	}
	
	@GetMapping("/SupprimerEquipe")
	public String supprimerEquipe(
			@RequestParam(value = "id", required = true) Integer id,
			Model model) {
		adminEquipe.supprimerEquipe(id);
		return pageAdminEquipeSetUp(model);
	}
	
	private String pageAdminEquipeSetUp(Model model) {
		// clean-up the textbox
		model.addAttribute("equipe", new Equipe());
		model.addAttribute("equipes", adminEquipe.listerEquipes());
		model.addAttribute("etudiant", new Etudiant());
		model.addAttribute("etudiants", adminEquipe.listerEtudiants());
		model.addAttribute("entreprises", adminEntreprise.listerEntreprisesDisponibles());
		return "AdminEquipe";
	}

	@PostMapping("/AdminEtudiant")
	public String ajouterEtudiant(
			Integer idEquipe,
			@Valid Etudiant etudiant,
			BindingResult result,
			Model model) {
		if (result.hasErrors()) {
			//TODO manage errors
			System.err.println(result.toString());
			
			return pageAdminEquipeSetUp(model);
		}
		etudiant.setId(adminEquipe.getNextId());
		
		// affecter l'étudiant à son équipe
		Equipe equipe = adminEquipe.getById(idEquipe);
		
		adminEquipe.ajouterEtudiant(etudiant, equipe);
	
		return pageAdminEquipeSetUp(model);
	}
	
	@GetMapping("/SupprimerEtudiant")
	public String supprimerEtudiant(
			@RequestParam(value = "id", required = true) Integer id,
			@RequestParam(value = "idEquipe", required = true) Integer idEquipe,
			Model model) {
		adminEquipe.supprimerEtudiant(id, idEquipe);
		return pageAdminEquipeSetUp(model);
	}
}
