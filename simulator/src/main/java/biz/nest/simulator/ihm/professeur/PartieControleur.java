/**
 * Copyleft 2019 - Vincent Mahé - vmahe@free.fr
 * Eclipse Public License 1.0
 */
package biz.nest.simulator.ihm.professeur;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import biz.nest.simulator.modele.Modele;
import biz.nest.simulator.modele.simulation.Partie;
import biz.nest.simulator.services.admin.AdminPartie;

/**
 * @author Vincent Mahé
 *
 */
@Controller
public class PartieControleur {

	@Autowired
	AdminPartie adminPartie;
	
	@GetMapping("/AdminPartie")
	public String adminPartie(Model model) {
		// nouvelle arrivée sur la page => on lui passe la partie en cours
		Partie partie = Modele.getInstance().getPartie();
		if (partie == null) {
			partie = new Partie();
			Modele.getInstance().setPartie(partie);
		}
		model.addAttribute("partie", partie);
		return "AdminPartie";
	}
	
	@PostMapping("/NouvellePartie")
	public String ajouterPartie(
			@Valid Partie partie,
			BindingResult result,
			Model model) {
		
		if (result.hasErrors()) {
			//TODO gérer les erreurs
			model.addAttribute("partie", new Partie());
			return "AdminPartie";
		}
		
		Modele.getInstance().setNouvellePartie(partie);
		
		model.addAttribute("partie", partie);
		return "AdminPartie";
	}
}
