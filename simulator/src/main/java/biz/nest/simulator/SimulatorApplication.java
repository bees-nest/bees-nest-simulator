package biz.nest.simulator;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.embedded.tomcat.ConnectorStartFailedException;

import biz.nest.simulator.util.OutilsServeur;

@SpringBootApplication
public class SimulatorApplication {

	public static void main(String[] args) {
		// if Tomcat is running, reopen the browser
		try {
			SpringApplication.run(SimulatorApplication.class, args);
		} catch (ConnectorStartFailedException e) {
			// on passe et on se contente de réafficher la page principale
		}
		
		// open the user browser on application main page
		String port = OutilsServeur.portServeur();
		BareBonesBrowserLaunch.openURL("http://localhost:" + port);
		
		//TODO manage the full stop of Tomcat server
	}
}
