#!/bin/sh

# nettoyer le projet
./gradlew clean

# générer le jar
./gradlew bootJar

# démarrer l'application
cd build/libs
java -jar simulator-0.0.1-SNAPSHOT.jar
